import cgi
import urllib
import os
import re
import jinja2
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'])
    
class MainPage(webapp2.RequestHandler):

    def get(self):
        template_values = { 'rpp_name': 'filename.rpp', }
        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(template_values))
        
class RppHelp(webapp2.RequestHandler):

    def get(self):
        template_values = { 'rpp_name': 'filename.rpp', }
        template = JINJA_ENVIRONMENT.get_template('rpp_help.html')
        self.response.write(template.render(template_values))
        
class RppFile(webapp2.RequestHandler):
    def _parse(self, buffer):
        class track:
            def __init__(self, track_name, number, indent):
                self.name = track_name
                self.number = number
                self.indent = indent
                self.vsts = []
                self.send_names = []
                self.recvs = []
                self.recv_names = []
                self.extras = False
                self.notes = None
                
        class vst:
            def __init__(self, vst_name, exe):
                self.name = vst_name
                self.exe = exe
                
        all_tracks = []
        
        re_name = re.compile('NAME \"(.+?)\"')
        re_vst = re.compile('<VST \"(.+?)\" \"(.+?)\"')
        re_track = re.compile('<TRACK')
        re_notes = re.compile('<NOTES')
        re_master = re.compile('<MASTERFXLIST')
#For most tracks, ISBUS 0 0
#For the parent track of a group, ISBUS 1 1
#For the last track in the group, ISBUS 2 -1
        re_isbus = re.compile('ISBUS (\d) (-?\d)')
        re_recv = re.compile('AUXRECV (\d)')
        track_number = 0
        lines = buffer.splitlines()
        get_track = False
        current_track = None
        track_indent = False
        in_notes = False
        for line in lines:
            if get_track:
                get_track = False
                m = re_name.search(line)
                if m:
                    track_number += 1
                    current_track = track(track_name=m.group(1), number=track_number, indent = track_indent)
                    all_tracks.append(current_track)
                    continue
            if in_notes:
                if line.strip() == '>':
                    in_notes = False
                else:
                    if current_track:
                        if current_track.notes:
                            current_track.notes += re.sub('\|', '', line)
                        else:
                            current_track.notes = re.sub('\|', '', line)
                continue
                
            m = re_master.search(line)
            if m:
                # track 0 is reserved for Master FX
                current_track = track(track_name='Master', number=0, indent = False)
                all_tracks.append(current_track)
                continue
                
            m = re_vst.search(line)
            if m:
                current_vst = vst(vst_name = m.group(1), exe = m.group(2))
                if current_track:
                    current_track.vsts.append(current_vst)
                continue

            m = re_isbus.search(line)
            if m:
                if m.group(1) == '1':
                    track_indent = True
                elif m.group(1) == '2':
                    track_indent = False
                continue

            m = re_recv.search(line)
            if m:
                if current_track:
                    current_track.recvs.append(m.group(1))
                continue
                
            m = re_notes.search(line)
            if m:
                in_notes = True
                continue
                
            if re_track.search(line):
                get_track = True

        # populate the sends
        for (index, track) in enumerate(all_tracks):
            for recv in track.recvs:
                track.recv_names.append("%d %s" % (int(recv) + 1, all_tracks[int(recv)].name))
                all_tracks[int(recv)].send_names.append("%d %s" % (index + 1, track.name))
                
        for track in all_tracks:
            if len(track.send_names) != 0 or len(track.recv_names) != 0 or track.notes:
                track.extras = True
            else:
                track.extras = False
        return all_tracks
        
    def post(self):
        if self.request.POST.get('help', None):
            template = JINJA_ENVIRONMENT.get_template('rpp_help.html')
            self.response.write(template.render())
        else:
            upload_file = self.request.get('rpp_file')
            if not upload_file or upload_file == '':
                filename = 'No file chosen'
                parsed = None
            else:
                filename = self.request.params['rpp_file'].filename
                parsed = self._parse(upload_file)
            template_values = { 'tracks': parsed, 'rpp_name': filename }
            template = JINJA_ENVIRONMENT.get_template('index.html')
            self.response.write(template.render(template_values))

        
application = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/rpp_name', RppFile),
    ('/rpp_help', RppHelp)
    ], debug=True)
    
